#!/usr/bin/perl -w

use Test::More 'no_plan';

package Catch;

sub TIEHANDLE {
    my($class, $var) = @_;
    return bless { var => $var }, $class;
}

sub PRINT  {
    my($self) = shift;
    ${'main::'.$self->{var}} .= join '', @_;
}

sub OPEN  {}    # XXX Hackery in case the user redirects
sub CLOSE {}    # XXX STDERR/STDOUT.  This is not the behavior we want.

sub READ {}
sub READLINE {}
sub GETC {}
sub BINMODE {}

my $Original_File = 'lib/Local/METAR.pm';

package main;

# pre-5.8.0's warns aren't caught by a tied STDERR.
$SIG{__WARN__} = sub { $main::_STDERR_ .= join '', @_; };
tie *STDOUT, 'Catch', '_STDOUT_' or die $!;
tie *STDERR, 'Catch', '_STDERR_' or die $!;

{
    undef $main::_STDOUT_;
    undef $main::_STDERR_;
#line 341 lib/Local/METAR.pm

use Local::METAR;
our $metar = new Local::METAR;
is(defined $metar => 1, 'Create object');


    undef $main::_STDOUT_;
    undef $main::_STDERR_;
}

{
    undef $main::_STDOUT_;
    undef $main::_STDERR_;
#line 355 lib/Local/METAR.pm

$metar->parse('METAR EHAM 121155Z 27006KT 9000 OVC005 07/06 Q1032');
is(
  $metar->localtime_string =~ /^12-\w\w\w\-\d\d\d\d \d\d:\d5$/ => 1,
  'Local time string',
);


    undef $main::_STDOUT_;
    undef $main::_STDERR_;
}

{
    undef $main::_STDOUT_;
    undef $main::_STDERR_;
#line 413 lib/Local/METAR.pm

my $string = $metar->human_readable;
print $string;
is($string =~ /^Amsterdam\/Schiphol/ => 1, 'Location');
is($string =~ /- wind west,/         => 1, 'Wind Direction');
is($string =~ /2\240Bft/             => 1, 'Wind Force');
is($string =~ /dauwpunt 6\240\260C/  => 1, 'Temperature/Dew Point');
is($string =~ /1032\240hPa/          => 1, 'Pressure');


    undef $main::_STDOUT_;
    undef $main::_STDERR_;
}

{
    undef $main::_STDOUT_;
    undef $main::_STDERR_;
#line 529 lib/Local/METAR.pm

is(Local::METAR::_format_thousands('001,000'), '1.000', 'Format Thousands');


    undef $main::_STDOUT_;
    undef $main::_STDERR_;
}

