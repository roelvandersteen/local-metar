# $Id$
package Local::METAR::Compact;
use base Local::METAR;
use 5.005;
use strict;
use warnings;
use Carp;

=head1 NAME

Local::METAR::Compact - Readable, but compact METAR information

=head1 SYNOPSIS

  use Local::METAR::Compact;
  my $metar = new Local::METAR::Compact;
  $metar->parse('METAR EHAM 121155Z 27006KT 9000 OVC005 07/06 Q1032');
  print $metar->human_readable;

=head1 WARNING

This class is a dirty hack. I'm going to clean-up one day. Promise.

=cut

BEGIN {
  our $VERSION = 0.01;
}

sub human_readable {
  my $self = shift;
  croak 'Must be called on object' unless ref($self);

  my @txt_parts = ();
  push @txt_parts, 'geen waarneming'                             if $self->nil;
  push @txt_parts, 'wind ' . $self->wind_direction    if $self->wind_direction;
  push @txt_parts, '(' . $self->variable_wind . ')'    if $self->variable_wind;
  push @txt_parts, $self->wind_force                      if $self->wind_force;
  push @txt_parts, '(uitschieters tot ' .
                   $self->gusts_force . ')'              if $self->gusts_force;
  push @txt_parts, 'zicht ' . $self->visibility           if $self->visibility
                                             && $self->visibility_meters < 500;
  push @txt_parts, $self->present_weather            if $self->present_weather;
  push @txt_parts, 'na ' . $self->recent_weather      if $self->recent_weather;
  if ($self->clouds) {
    my @clouds = @{$self->clouds};
    @clouds = grep { ! /licht/ } @clouds if grep /half|zwaar|geheel/, @clouds;
    push @txt_parts, @clouds;
  }
# push @txt_parts, 'verticaal zicht ' .
                   $self->vertical_visibility    if $self->vertical_visibility;
  push @txt_parts, 'temperatuur ' . $self->temperature   if $self->temperature;
# push @txt_parts, 'dauwpunt ' . $self->dew_point          if $self->dew_point;
 push @txt_parts, 'luchtdruk ' . $self->pressure           if $self->pressure;
  push @txt_parts, $self->trend->human_readable                if $self->trend;

  if ($self->trend_indicator) {
    if ($self->trend_indicator eq 'NOSIG') {
      return $self->trend_text;
    } elsif ( @txt_parts ) {
      return _myjoin( ' ', _myjoin(' ', $self->trend_text, $self->trend_time ),
        join(', ', @txt_parts) );
    } else {
      return '';
    }
  } else {
    return sprintf("%02u:%02u - ", (localtime($self->epoch_seconds))[2, 1]) .
      _myjoin(', ', @txt_parts) . '.';
  }
}

sub _myjoin { Local::METAR::_myjoin @_ }

1; # don't forget to return TRUE from module

__END__

=head1 AUTHOR

Roel van der Steen, roel-perl@st2x.net

=head1 COPYRIGHT AND LICENSE

Copyright 2004 by Roel van der Steen

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=cut
