# $Id$
package Local::METAR;
use 5.005;
use strict;
use warnings;
use Carp;
use Data::Dump;
use Time::Local;
my $debugging = 0;

=head1 NAME

Local::METAR - Parse METAR data and convert to human readable form

=head1 SYNOPSIS

  use Local::METAR;
  my $metar = new Local::METAR;
  $metar->parse('METAR EHAM 121155Z 27006KT 9000 OVC005 07/06 Q1032');
  print $metar->human_readable;

=cut

BEGIN {
  our $VERSION = 0.01;
}

my %locations = (
  EHAM   => 'Amsterdam/Schiphol',
  EHAU   => 'AUK-A Platform',
  EHBK   => 'Maastricht/Beek',
  EHDB   => 'De Bilt',
  EHDL   => 'Arnhem/Deelen',
  EHDV   => 'D15-FA-1 Platform',
  EHEH   => 'Eindhoven',
  EHFD   => 'F3-FB-1 Platform',
  EHFS   => 'Vlissingen',
  EHFZ   => 'F16-A Platform',
  EHGG   => 'Groningen/Eelde',
  EHGO   => 'Goeree Lichteiland',
  EHGR   => 'Breda/Gilze-Rijen',
  EHKA   => 'K13-A Platform',
  EHKD   => 'Den Helder/De Kooy',
  EHKV   => 'K14-FA-1C Platform',
  EHLE   => 'Lelystad',
  EHLW   => 'Leeuwarden',
  EHMA   => 'AWG-1 Platform',
  EHMG   => 'L9-FF-1 Platform',
  EHMP   => 'Noordwijk Meetpost',
  EHNC   => 'Noordelijke Zeeraaf Platform',
  EHRD   => 'Rotterdam/Zestienhoven',
  EHSA   => 'Euro Platform',
  EHSB   => 'Utrecht/Soesterberg',
  EHTW   => 'Enschede/Twente',
  EHVB   => 'Leiden/Valkenburg',
  EHVK   => 'Uden/Volkel',
  EHVL   => 'Vlieland',
  EHWO   => 'Woensdrecht',
);
my $location_list = join '|', keys(%locations);

my %wthr_phen = (
  BC     => 'banken',
  BL     => 'hoog opwaaiend',
  BR     => 'nevel en',
  DR     => 'laag opwaaiend',
  DU     => 'verspreid stof en',
  DZ     => 'motregen en',
  FC     => 'wind- of waterhoos en',
  FG     => 'mist en',
  FZ     => 'onderkoeld',
  FU     => 'rook en',
  GR     => 'grove hagel en',
  GS     => 'kleine hagel en',
  IC     => 'ijskristallen en',
  HZ     => 'heiig en',
  LN     =>  'weerlicht en',
  MI     => 'laaghangend',
  PE     => 'ijsregen en',
  PO     => 'stofhoos en',
  PR     => 'gedeeltelijk',
  RA     => 'regen en',
  SA     => 'zand en',
  SG     => 'motsneeuw en',
  SH     => 'bui en',
  SN     => 'sneeuw en',
  SQ     => 'zware windstoten en',
  TS     => 'onweer en',
  VC     => 'in de omgeving',
);
my $wthr_phen_list = join '|', keys(%wthr_phen);

my %clds_cvr = (
  CLR    => 'helder',
  SKC    => 'helder',
  NSC    => 'geen significante bewolking',
  FEW    => 'licht bewolkt',
  SCT    => 'half bewolkt',
  BKN    => 'zwaar bewolkt',
  OVC    => 'geheel bewolkt',
);
my $cld_cvr_list = join '|', keys(%clds_cvr);

my %clds_type = (
  CB     => 'cumulonimbus',
  TCU    => 'cumulus met grote verticale opbouw',
);
my $cld_type_list = join '|', keys(%clds_type);

my %colourstate = (
  BLU    => 'zicht >�8�km en/of laagste wolkenbasis >�2.500�voet',
  WHT    => 'zicht 5-8�km en/of laagste wolkenbasis 1.500-2.500�voet',
  GRN    => 'zicht 3.700-5.000�m en/of laagste wolken 700-1.500�voet',
  YLO    => 'zicht 1.600-3.700�m en/of laagste wolken 300-700�voet',
  AMB    => 'zicht 800-1.600�m en/of laagste wolken 200-300�voet',
  RED    => 'zicht <�800�m en/of laagste wolken <�200�voet',
);
my $colourstate_list = join '|', keys(%colourstate);

my %tokens = (

  # message type
  qr/(METAR|SPECI)/ =>
  sub {
    my ($self, $type) = @_[0 .. 1];
    $self->mess_type_id($type);
    $self->mess_type   ( ($type =~ /METAR/) ? 'Regulier rapport'
                                            : 'Speciaal rapport' );
  },

  # nil identifier
  qr/NIL/ =>
  sub {
    my $self = $_[0];
    $self->nil('nil');
  },

  # auto identifier
  qr/AUTO/ =>
  sub {
    my $self = $_[0];
    $self->auto('auto');
  },

  # location identifier
  qr/($location_list)/ =>
  sub {
    my ($self, $l_id) = @_[0 .. 1];
    $self->location_id($l_id);
    $self->location   ($locations{$l_id});
  },

  # date/time group
  qr/(\d\d)(\d\d)(\d\d)Z/ =>
  sub {
    my ($self, $mday, $hrs, $mins) = @_[0 .. 3];
    my @mtime = gmtime;
    @mtime[0 .. 3] = (0, $mins, $hrs, $mday); # replace secs, mins, hrs, mday
    if ( timegm(@mtime) > time) { # result in future? => assume previous month
      if (--$mtime[4] < 0) { $mtime[4] = 11; --$mtime[5] }
    }
    $self->epoch_seconds   (timegm(@mtime));
    $self->localtime_string(_localtime_string($self->epoch_seconds));
  },

  # wind group
  qr/(\d\d\d|VRB)(\d\d)(G\d\d)?(KMH|KT|MPS)/ =>
  sub {
    my ($self, $dir, $speed, $gusts, $unit) = @_[0 .. 4];
    if ( $speed > 0 ) {
      $self->wind_degrees  ($dir);
      $self->wind_direction(_direction_string($dir));
    } else {
      $self->wind_direction('windstil');
    }
    $self->wind_speed   ($unit eq 'KMH' ? $speed / 1.852   :
                         $unit eq 'MPS' ? $speed / 0.51444 : $speed );
    $self->wind_force   (_knots_to_bft($self->wind_speed));
    if ($gusts) {
      $gusts =~ s/G//;
      $self->gusts_speed($unit eq 'KMH' ? $gusts / 1.852   :
                         $unit eq 'MPS' ? $gusts / 0.51444 : $gusts );
      $self->gusts_force(_knots_to_bft($self->gusts_speed));
    }
  },

  # variable wind group
  qr/(\d\d\d)V(\d\d\d)/ =>
  sub {
    my ($self, $from, $to) = @_[0 .. 2];
    $self->variable_wind_degrees( [$from, $to] );
    $self->variable_wind( sprintf 'variabel tussen %s en %s',
      _direction_string($from), _direction_string($to) );
  },

  # visibility group
  qr/(CAVOK|\d\d\d\d)/ =>
  sub {
    my ($self, $vis) = @_[0 .. 1];
    $self->visibility_meters($vis eq 'CAVOK' ? 9999 : $vis);
    if ($vis eq 'CAVOK') {
      $self->visibility('>�10�km en geen wolken <�5.000�voet');
    } elsif ($vis == 0000 ) {
      $self->visibility('<�50�meter');
    } elsif ($vis == 9999 ) {
      $self->visibility('>�10�km');
    } else {
      $self->visibility(_format_thousands($vis) . '�meter');
    }
  },

  # present weather group
  qr/([+-])?((?:$wthr_phen_list)+)/ =>
  sub {
    my ($self, $mod, $wthr_itms) = @_[0 .. 2];
    my @wthr;
    my $ts = '';
    if ( defined $mod ) {
      if    ( $mod =~ /\+/ ) { $mod = 'hevige '; }
      elsif ( $mod =~  /-/ ) { $mod = 'lichte '; }
    } else {
      $mod = '';
    }
    while ( $wthr_itms =~ /\G($wthr_phen_list)/g ) {
      if ($1 eq 'TS' && $mod) { # thunderstorm exception
        $ts = $wthr_phen{$1};
      } else {
        push @wthr, $wthr_phen{$1};
      }
    }
    $wthr[-1] =~ s/ en$//; # strip off last "and"
    $self->present_weather('') unless $self->present_weather;
    $self->present_weather(
      $self->present_weather . " $ts$mod" . join ' ', @wthr
    );
  },

  # recent weather group
  qr/RE([+-])?((?:$wthr_phen_list)+)/ =>
  sub {
    my ($self, $mod, $wthr_itms) = @_[0 .. 2];
    my @wthr;
    my $ts = '';
    if ( defined $mod ) {
      if    ( $mod =~ /\+/ ) { $mod = 'hevige '; }
      elsif ( $mod =~  /-/ ) { $mod = 'lichte '; }
    } else {
      $mod = '';
    }
    while ( $wthr_itms =~ /\G($wthr_phen_list)/g ) {
      if ($1 eq 'TS' && $mod) { # thunderstorm exception
        $ts = $wthr_phen{$1};
      } else {
        push @wthr, $wthr_phen{$1};
      }
    }
    $wthr[-1] =~ s/ en$//; # strip off last "and"
    $self->recent_weather('') unless $self->recent_weather;
    $self->recent_weather($self->recent_weather . " $ts$mod" . join ' ', @wthr);
  },


  # clouds group(s)
  qr/($cld_cvr_list)(\d\d\d|\/\/\/)?($cld_type_list)?/ =>
  sub {
    my ($self, $cvr, $alt, $type) = @_[0 .. 3];
    $alt ||= 0;
    $self->clouds([ ]) unless defined $self->clouds;
    push @{$self->clouds}, _myjoin( ' ',
      $clds_cvr{$cvr},
      (defined $type ? '(' . $clds_type{$type} . ')' : ''),
      ($alt eq '///' ? 'op onbekende hoogte' :
       $alt == 0     ? '' : 'op ' . _format_thousands($alt * 100) . '�voet')
    );
  },

  # colour state group(s)
  qr/($colourstate_list)/ =>
  sub {
    my ($self, $colour) = @_[0 .. 1];
    $self->colourstate([ ]) unless defined $self->colourstate;
    push @{$self->colourstate}, $colour;
  },

  # vertical visibility group
  qr/VV(\d\d\d)/ =>
  sub {
    my ($self, $vis) = @_[0 .. 1];
    $self->vertical_visibility( sprintf '%s�voet',
      _format_thousands($vis * 100)
    );
  },

  # temperature/dew point group
  qr/(M?\d\d)\/(M?\d\d)/ =>
  sub {
    my ($self, $temp, $dew) = @_[0 .. 2];
    $temp =~ /(M)?(\d\d)/;
    $self->temperature(sprintf '%s%s��C', ($1 ? "\x{2212}" : ''), 1 * $2);
    $dew  =~ /(M)?(\d\d)/;
    $self->dew_point  (sprintf '%s%s��C', ($1 ? "\x{2212}" : ''), 1 * $2);
  },

  # atmospheric pressure group
  qr/(Q|A)(\d{4})/ =>
  sub {
    my ($self, $unit, $press) = @_[0 .. 2];
    $press =~ s/^0+//;
    $press /= 0.02953007 if $unit eq 'A';
    $self->pressure("$press�hPa");
  },

  # trend group
  qr/(BECMG|NOSIG|TEMPO)/ =>
  sub {
    my ($self, $trend) = @_[0 .. 1];
    $self->trend_indicator($trend);
    if ($trend eq 'BECMG') {
      $self->trend_text('wordt');
    } elsif ($trend eq 'NOSIG') {
      $self->trend_text('geen significante verandering verwacht');
    } elsif ($trend eq 'TEMPO') {
      $self->trend_text('tijdelijk');
    }
  },

  # trend time group
  qr/(AT|FM|TL)(\d\d)(\d\d)/ =>
  sub {
    my ($self, $time_indicator, $hrs, $mins) = @_[0 .. 3];
    my $tzdiff = (timegm(0, 0, 0, 1, 0, 0) - timelocal(0, 0, 0, 1, 0, 0)) / 60;
    my $local_mins = ($hrs * 60 + $mins + $tzdiff + 1440) % 1440;
    $hrs = int($local_mins / 60);
    $mins = $local_mins % 60;
    $mins = sprintf '%.2u', $mins;
    $self->trend_time('') unless $self->trend_time;
    $time_indicator = $time_indicator eq 'AT' ? 'om'    :
                      $time_indicator eq 'FM' ? 'vanaf' :
                      $time_indicator eq 'TL' ? 'tot'   : undef ;
    $self->trend_time(_myjoin(' ', $self->trend_time, $time_indicator, "$hrs:$mins"));
  }

);
print Data::Dump::dump(%tokens), "\n" if $debugging;

=begin testing

use Local::METAR;
our $metar = new Local::METAR;
is(defined $metar => 1, 'Create object');

=end testing

=cut

sub new {
  return bless( {}, ref($_[0]) || $_[0] );
}

=begin testing

$metar->parse('METAR EHAM 121155Z 27006KT 9000 OVC005 07/06 Q1032');
is(
  $metar->localtime_string =~ /^12-\w\w\w\-\d\d\d\d \d\d:\d5$/ => 1,
  'Local time string',
);

=end testing

=cut

sub parse {
  my $self = shift;
  croak 'Must be called on object' unless ref($self);

  # for easier parsing, remove space between time indicator and time
  (my $metar = uc join ' ', @_) =~ s/(AT|FM|TL)\s+(\d\d\d\d)/$1$2/g;

  # split off trend part
  my ($current, @trend) = split /(BECMG|NOSIG|TEMPO)/, $metar;

  my @current_list = split /\s+|=/, $current;
  my @trend_list = map { split /\s+|=/ } @trend;
  my @unparsable;

  CURRENT_PART:
  for my $current_part ( @current_list ) {
    for my $re (keys %tokens) {
      if ( $current_part =~ /^$re$/ ) {
        &{$tokens{$re}}($self, $1, $2, $3, $4, $5, $6, $7, $8, $9);
        next CURRENT_PART;
      }
    }
    push @unparsable, $current_part;
  }

  if (@trend_list) {
    $self->trend($self->new);
    TREND_PART:
    for my $trend_part ( @trend_list ) {
      for my $re (keys %tokens) {
        if ( $trend_part =~ /^$re$/ ) {
          &{$tokens{$re}}($self->trend, $1, $2, $3, $4, $5, $6, $7, $8, $9);
          next TREND_PART;
        }
      }
      push @unparsable, $trend_part;
    }
    $self->trend->metar (join ' ', @trend_list);
  }

  $self->current      (join ' ', @current_list);
  $self->metar        (join ' ', @current_list, @trend_list);
  $self->unparsable   (join ' ', @unparsable);
  return $self;
}

=begin testing

my $string = $metar->human_readable;
print $string;
is($string =~ /^Amsterdam\/Schiphol/ => 1, 'Location');
is($string =~ /- wind west,/         => 1, 'Wind Direction');
is($string =~ /2\240Bft/             => 1, 'Wind Force');
is($string =~ /dauwpunt 6\240\260C/  => 1, 'Temperature/Dew Point');
is($string =~ /1032\240hPa/          => 1, 'Pressure');

=end testing

=cut

sub human_readable {
  my $self = shift;
  croak 'Must be called on object' unless ref($self);

  my @txt_parts = ();
  push @txt_parts, 'geen waarneming'                             if $self->nil;
  push @txt_parts, 'wind ' . $self->wind_direction    if $self->wind_direction;
  push @txt_parts, '(' . $self->variable_wind . ')'    if $self->variable_wind;
  push @txt_parts, $self->wind_force                      if $self->wind_force;
  push @txt_parts, '(uitschieters tot ' .
                   $self->gusts_force . ')'              if $self->gusts_force;
  push @txt_parts, 'zicht ' . $self->visibility           if $self->visibility;
  push @txt_parts, $self->present_weather            if $self->present_weather;
  push @txt_parts, 'na ' . $self->recent_weather      if $self->recent_weather;
  push @txt_parts, @{$self->clouds}                           if $self->clouds;
  push @txt_parts, 'verticaal zicht ' .
                   $self->vertical_visibility    if $self->vertical_visibility;
  push @txt_parts, 'temperatuur ' . $self->temperature   if $self->temperature;
  push @txt_parts, 'dauwpunt ' . $self->dew_point          if $self->dew_point;
  push @txt_parts, 'luchtdruk ' . $self->pressure           if $self->pressure;
  push @txt_parts, $self->trend->human_readable                if $self->trend;

  if ($self->trend_indicator) {
    if ($self->trend_indicator eq 'NOSIG') {
      return $self->trend_text;
    } elsif ( @txt_parts ) {
      return _myjoin( ' ', _myjoin(' ', $self->trend_text, $self->trend_time ),
        join(', ', @txt_parts) );
    } else {
      return '';
    }
  } else {
    return _myjoin(' ', $self->location, $self->localtime_string) .
      ($self->auto ? ' (auto) - ' : ' - ') .
      _myjoin(', ', @txt_parts) . '.';
  }
}

sub AUTOLOAD {
  return if our $AUTOLOAD =~ /::DESTROY$/;
  my $self = shift;
  croak 'Must be called on object' unless ref($self);

  (my $attribute = $AUTOLOAD) =~ s/^.*:://;
  $self->{$attribute} = $_[0] if @_;
  return $self->{$attribute};
}

sub _myjoin {
  my $joiner = shift;
  my @elements = ();
  for (@_) {
    push @elements, $_ if defined $_ && $_ ne '';
  }
  return join($joiner, @elements);
}

sub _localtime_string {
  my @t = localtime(shift);
  my @months = qw(jan feb mrt apr mei jun jul aug sep okt nov dec);
  $t[5] += 1900 if $t[5] < 1000;
  return sprintf('%02u-%s-%04u %02u:%02u',
                  $t[3],$months[$t[4]], $t[5], $t[2],$t[1]);
}

sub _direction_string {
  my $deg = shift;
  return unless defined $deg;
  return 'variabel' if $deg =~ /VRB/;
  my $i = int( ($deg + 11.25) / 22.5 ) % 16;
  my @directions = qw(
    noord noord-noordoost noordoost oost-noordoost
    oost  oost-zuidoost   zuidoost  zuid-zuidoost
    zuid  zuid-zuidwest   zuidwest  west-zuidwest
    west  west-noordwest  noordwest noord-noordwest
  );
  return $directions[$i];
}

sub _knots_to_bft {
  my $kn = shift;
  return unless defined $kn;
  my @scale = (
    [ 0,  0],
    [ 1,  3],
    [ 4,  6],
    [ 7, 10],
    [11, 16],
    [17, 21],
    [22, 27],
    [28, 33],
    [34, 40],
    [41, 47],
    [48, 55],
    [56, 63],
    [63, 99],
  );
  for (my $bft = 0; $bft <= $#scale; $bft++) {
    return "$bft�Bft" if $kn >= $scale[$bft][0] && $kn <= $scale[$bft][1];
  }
}

=begin testing

is(Local::METAR::_format_thousands('001,000'), '1.000', 'Format Thousands');

=end testing

=cut

sub _format_thousands {
  (my $digits = shift) =~ s/\D//;
  $digits =~ s/^0+//;
  my @num = split //, $digits;
  for (my $i = $#num - 3; $i >= 0; $i -= 3) { $num[$i] .= '.' }
  return join '', @num;
}

1; # don't forget to return TRUE from module

__END__

=head1 AUTHOR

Roel van der Steen, roel-perl@st2x.net

=head1 COPYRIGHT AND LICENSE

Copyright 2004 by Roel van der Steen

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=cut
